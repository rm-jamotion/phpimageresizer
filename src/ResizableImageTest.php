<?php
/**
 * Created by JetBrains PhpStorm.
 * User: renzo.meister
 * Date: 11.06.13
 * Time: 14:03
 * To change this template use File | Settings | File Templates.
 */
require('ResizableImage.php');

class ResizableImageTest extends PHPUnit_Framework_TestCase
{

    private $_testImages = 'images/test/';
    private $_resultImages = 'images/test/results/';
    private $_sourcesPNG = array(
        'testimage100x100.png' => array(100, 100),
        'testimage150x200.png' => array(150, 200),
        'testimage200x150.png' => array(200, 150)
    );
    private $_sourcesJPG = array(
        'testimage100x100.jpg' => array(100, 100),
        'testimage150x200.jpg' => array(150, 200),
        'testimage200x150.jpg' => array(200, 150)
    );
    private $_targets = array(
        array(50, 50), array(100, 100), array(200, 200), array(300, 300), array(400, 400),
        array(50, 100), array(100, 200), array(200, 400), array(300, 600), array(400, 800),
        array(100, 50), array(200, 100), array(400, 200), array(600, 300), array(800, 400),
        array(50, 75), array(100, 150), array(200, 275), array(300, 200), array(400, 550),
        array(75, 50), array(150, 100), array(275, 200), array(200, 300), array(550, 400),
        array(200, null), array(null, 200)
    );

    /**
     * @expectedException InvalidArgumentException
     */
    public function testResizeBMPWithException()
    {
        $img = new ResizableImage($this->_testImages . 'testimage.bmp');
    }

    public function testResizeImageFromPath()
    {
        $img = new ResizableImage($this->_testImages);
        $this->assertEquals(150, $img->getWidth());
    }

    public function testResizeFileNotFoundUseUnknownImage()
    {
        $img = new ResizableImage($this->_testImages . 'testimage.unknown');
        $img->resize();
        $this->assertEquals(150, $img->getWidth());
    }

    public function testResizeFileNotFoundUseEmptyImage()
    {
        $img = new ResizableImage($this->_testImages . 'testimage.unknown', false);
        $img->resize();
        $this->assertEquals(150, $img->getWidth());
    }

    public function testResizePNGImages()
    {

        foreach ($this->_sourcesPNG as $src => $srcXY) {
            foreach ($this->_targets as $tgt) {

                $img = new ResizableImage($this->_testImages . $src);
                $img->setBackgroundColorRGBA(-1, 270, 0, -1);
                $color = $img->getBackgroundColor();
                $this->assertEquals(0, $color[0]);
                $this->assertEquals(255, $color[1]);
                $this->assertEquals(0, $color[3]);

                $img->setBackgroundColorRGBA(0, 0, 0, 255);
                $color = $img->getBackgroundColor();
                $this->assertEquals(127, $color[3]);

                $this->assertNotNull($img);
                $this->assertInstanceOf('ResizableImage', $img);
                $this->assertEquals($srcXY[0], $img->getWidth());
                $this->assertEquals($srcXY[1], $img->getHeight());

                $img->resize($tgt[0], $tgt[1]);

                if (!is_null($tgt[0])) {
                    $this->assertEquals($tgt[0], $img->getMaxWidth());
                    $this->assertEquals($tgt[0], $img->getWidth());
                }

                if (!is_null($tgt[1])) {
                    $this->assertEquals($tgt[1], $img->getMaxHeight());
                    $this->assertEquals($tgt[1], $img->getHeight());
                }

                $outPath = $this->_resultImages . 'png/';
                if (!file_exists($outPath))
                    mkdir($outPath,0777,true);

                $outputImage = $outPath . sprintf("result_%s_%u_%u.png", $src, $tgt[0], $tgt[1]);
                if (file_exists($outputImage))
                    unlink($outputImage);
                $img->getOrSaveAs('png', $outputImage);
            }
        }
    }

    public function testResizeJPGImages()
    {

        foreach ($this->_sourcesJPG as $src => $srcXY) {
            foreach ($this->_targets as $tgt) {

                $img = new ResizableImage($this->_testImages . $src);
                $img->setBackgroundColorHex("#0000");
                $color = $img->getBackgroundColor();
                $this->assertEquals(0, $color[0]);
                $img->setBackgroundColorHex("#00000000");
                $color = $img->getBackgroundColor();
                $this->assertEquals(0, $color[0]);
                $this->assertNotNull($img);
                $this->assertInstanceOf('ResizableImage', $img);
                $this->assertEquals($img->getWidth(), $srcXY[0]);
                $this->assertEquals($img->getHeight(), $srcXY[1]);

                $img->resize($tgt[0], $tgt[1]);

                if (!is_null($tgt[0])) {
                    $this->assertEquals($tgt[0], $img->getMaxWidth());
                    $this->assertEquals($tgt[0], $img->getWidth());
                }

                if (!is_null($tgt[1])) {
                    $this->assertEquals($tgt[1], $img->getMaxHeight());
                    $this->assertEquals($tgt[1], $img->getHeight());
                }

                $outPath = $this->_resultImages . 'jpg/';
                if (!file_exists($outPath))
                    mkdir($outPath,0777,true);

                $outputImage = $outPath . sprintf("result_%s_%u_%u.jpg", $src, $tgt[0], $tgt[1]);
                if (file_exists($outputImage))
                    unlink($outputImage);
                $img->getOrSaveAs('jpg', $outputImage);
            }
        }
    }
}
