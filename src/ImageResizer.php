<?php
/**
 * Created by JetBrains PhpStorm.
 * User: renzo.meister
 * Date: 13.06.13
 * Time: 09:11
 * To change this template use File | Settings | File Templates.
 *
 * This file should be called directly as img source
 * @Example
 * <img src="ImageResizer.php?image=filename" />
 */

/**************************************************
 * Configuration of ImageResizer
 **************************************************/
// Default Image Path
define('IMAGE_PATH', '../wwwhowegneu/images');

// Usage of Unknown Image if given file don't exist
if (isset($_GET['test']) && $_GET['test'] == "1")
    define('USE_UNKNOWN_IMAGE', true);
else
    define('USE_UNKNOWN_IMAGE', false);

/************************************
 * Read additional URL Parameters
 ************************************/

// Filepath of image
$file = isset($_GET['image']) ? $_GET['image'] : null;

// Maximum width of resampled image
$width = $_GET['width'];

// Maximum height of resampled image
$height = $_GET['height'];

// Background color used for emtpy spaces produced while proportional resampling
$color = isset($_GET['color']) ? $_GET['color'] : null;

// Result type (only 'png' or 'jpeg' are allowed)
$type = strtolower(isset($_GET['type']) ? $_GET['type'] : 'png');


/***************************
 * Start image resampling
 ***************************/
// concatenate full file path of image
$filePath = is_null($file) ? 'images/usage.png' : IMAGE_PATH . DIRECTORY_SEPARATOR . $file;

// Set the content type to image
header("Content-type: image/" . $type);

// Load the ResizableImage Helper Class
require('ResizableImage.php');

// Load the image as ResizableImage object
$img = new ResizableImage($filePath, USE_UNKNOWN_IMAGE);

// Set the default background color for empty spaces
$img->setBackgroundColorHex($color);

// Resize the image
$img->resize($width, $height);

// Send resized image content to stdout
$img->getOrSaveAs($type);

