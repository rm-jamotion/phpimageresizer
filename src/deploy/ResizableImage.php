<?php
/**
 * Project: ImageResizer
 * User:    Renzo Meister
 * Date:    12.06.2013
 * Time:    14:00
 */

/**
 * Class ResizableImage
 *
 * This Class provides image features for resizing an image.
 */
class ResizableImage
{
    private $_imageSource = null;
    private $_sourceImage = null;
    private $_resampledImage = null;
    private $_maxWidth = 0;
    private $_maxHeight = 0;
    private $_sourceWidth = 0;
    private $_resampledWidth = 0;
    private $_sourceHeight = 0;
    private $_resampledHeight = 0;
    private $_backgroundColor = array();
    private $_useUnknownImage = true;

    /**
     * Constructor with direct load of an image
     * Only JPEG and PNG images are supported yet
     *
     * @param string $imageSource file path of an image file
     */
    public function __construct($imageSource, $useUnknownImage = true)
    {
        // default background color = white and fully transparent
        $this->_backgroundColor[0] = 255; // Red
        $this->_backgroundColor[1] = 255; // Green
        $this->_backgroundColor[2] = 255; // Blue
        $this->_backgroundColor[3] = 127; // Alpha

        $this->_useUnknownImage = $useUnknownImage;

        if ((!file_exists($imageSource) || !is_file($imageSource)) && $this->_useUnknownImage) {
            $this->_imageSource = 'images/unknown.png';
        } elseif (file_exists($imageSource) && is_file($imageSource)) {
            $this->_imageSource = $imageSource;
        } else {
            // don't load any image because of using an empty image as result
            return;
        }
        $this->loadImage();

    }

    /**
     * resizes the image based on given maximum size
     *
     * the image always holds the proportions. If new image is higher or wider then original
     * the original image will be centered and the free space is filled with a given color or
     * with transparent color (only supported on PNG output)
     *
     * @param int $maxWidth  maximum width of new image
     * @param int $maxHeight maximum height of new image
     */
    public function resize($maxWidth = 0, $maxHeight = 0)
    {

        if (is_null($this->_imageSource)) {
            // no source image found so we create an empty image

            // set maximum width and height
            $this->_maxWidth = $maxWidth == 0 ? 150 : $maxWidth;
            $this->_maxHeight = $maxHeight == 0 ? 150 : $maxHeight;

            // create the new image as empty
            $this->_resampledImage = imagecreatetruecolor($this->_maxWidth, $this->_maxHeight);

            // fill new image with background color
            imagefill($this->_resampledImage, 0, 0, $this->allocateColor());

            // set width and height to maxWidth and maxHeight
            $this->_resampledWidth = $this->_maxWidth;
            $this->_resampledHeight = $this->_maxHeight;
            return;
        }

        if ($maxWidth == 0 && $maxHeight == 0) {
            // No width or height given, set the original image as result
            $this->_maxWidth = $this->_sourceWidth;
            $this->_maxHeight = $this->_sourceHeight;
            $this->_resampledImage = $this->_sourceImage;
            return;

        } elseif ($maxWidth == 0) {
            // no width given, calculate the width based on given height and ratio of original image
            $this->_maxHeight = $maxHeight;
            $this->_maxWidth = $this->_maxHeight * ($this->_sourceWidth / $this->_sourceHeight);

        } elseif ($maxHeight == 0) {
            // no height given, calculate the height based on given width and ratio of original image
            $this->_maxWidth = $maxWidth;
            $this->_maxHeight = $this->_maxWidth * ($this->_sourceHeight / $this->_sourceWidth);

        } else {
            // both values (height and width) given, use them
            $this->_maxWidth = $maxWidth;
            $this->_maxHeight = $maxHeight;

        }

        // calculate width and height ratios for proportional image resize and positioning
        $widthRatio = $this->_maxWidth / $this->_sourceWidth;
        $heightRatio = $this->_maxHeight / $this->_sourceHeight;

        // calculate size ratio for growing or shrinking original image
        $sizeRatio = $widthRatio < $heightRatio ? $widthRatio : $heightRatio;

        // now calculate the position and size of original image in new image
        $destWidth = $this->_sourceWidth * $sizeRatio;
        $destHeight = $this->_sourceHeight * $sizeRatio;
        $destLeft = ($this->_maxWidth - $destWidth) / 2;
        $destTop = ($this->_maxHeight - $destHeight) / 2;

        // create the new image as empty
        $this->_resampledImage = imagecreatetruecolor($this->_maxWidth, $this->_maxHeight);

        // fill new image with a default background color
        imagefill($this->_resampledImage, 0, 0, $this->allocateColor());

        // copy original image into new image with new position and width
        imagecopyresampled($this->_resampledImage, $this->_sourceImage,
            $destLeft, $destTop,
            0, 0,
            $destWidth, $destHeight,
            $this->_sourceWidth, $this->_sourceHeight);

        // read the current image width and height
        $this->_resampledWidth = imagesx($this->_resampledImage);
        $this->_resampledHeight = imagesy($this->_resampledImage);
    }

    /**
     * @return int
     */
    public function allocateColor()
    {
        $bgColor = imagecolorallocatealpha($this->_resampledImage,
            $this->_backgroundColor[0],
            $this->_backgroundColor[1],
            $this->_backgroundColor[2],
            $this->_backgroundColor[3]
        );
        return $bgColor;
    }

    /**
     * Gets the width of current image
     *
     * @return int the width in pixel
     */
    public function getWidth()
    {
        return $this->_resampledWidth != null ? $this->_resampledWidth : $this->_sourceWidth;
    }

    /**
     * Gets the height of current image
     *
     * @return int the height in pixel
     */
    public function getHeight()
    {
        return $this->_resampledHeight != null ? $this->_resampledHeight : $this->_sourceHeight;
    }

    /**
     * Gets the defined maximum width
     *
     * @return int the maximum width in pixel
     */
    public function getMaxWidth()
    {
        return $this->_maxWidth;
    }

    /**
     * Gets the defined maximum height
     *
     * @return int the maximum height in pixel
     */
    public function getMaxHeight()
    {
        return $this->_maxHeight;
    }

    /**
     * Writes the image to stdout or into a file based on given type
     * only JPEG and PNG are supported yet
     *
     * @param string $type   image type (allowed values: 'png' or 'jpg' / 'jpeg')
     * @param null|string $path   file path if the image should be saved, instead of sending to stdout (default)
     */
    public function getOrSaveAs($type, $path = null)
    {
        switch (strtolower($type)) {
            default:
                $this->getOrSaveAsJPEG($path);
                break;
            case 'png':
                $this->getOrSaveAsPNG($path);
                break;
        }
    }

    /**
     * Writes the image to stdout or into a file in JPEG format
     *
     * @param null|string $path  file path if the image should be saved, instead of sending to stdout (default)
     */
    public function getOrSaveAsJPEG($path = null)
    {
        imagesavealpha($this->_resampledImage, true);
        imagejpeg($this->_resampledImage, $path);
    }

    /**
     * Writes the image to stdout or into a file in PNG format
     *
     * @param null|string $path  file path if the image should be saved, instead of sending to stdout (default)
     */
    public function getOrSaveAsPNG($path = null)
    {
        imagesavealpha($this->_resampledImage, true);
        imagepng($this->_resampledImage, $path);
    }

    /**
     * Set the background color (as HEX string) used for empty spaces
     * if ratio has changed between original and new image
     *
     * @param string $hex  Color value as hex string with length of 3,4,6 or 8 chars (leading # possible)
     */
    public function setBackgroundColorHex($hex)
    {

        $hex = str_replace('#', '', $hex);

        $color = array();

        if (strlen($hex) == 3 || strlen($hex) == 4) {
            $color[0] = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $color[1] = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $color[2] = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
            $color[3] = strlen($hex) == 4 ? hexdec(substr($hex, 3, 1) . substr($hex, 3, 1)) : $color[3] = 0;
        } else if (strlen($hex) == 6 || strlen($hex) == 8) {
            $color[0] = hexdec(substr($hex, 0, 2));
            $color[1] = hexdec(substr($hex, 2, 2));
            $color[2] = hexdec(substr($hex, 4, 2));
            $color[3] = strlen($hex) == 8 ? hexdec(substr($hex, 6, 2)) : $color[3] = 0;
        }

        if (count($color) == 4)
            $this->setBackgroundColorRGBA($color[0], $color[1], $color[2], $color[3]);
    }

    /**
     * Returns the defined background color as array
     *
     * @return array (0 => r, 1 => g, 2 => b, 3 => Alpha)
     */
    public function getBackgroundColor()
    {
        return $this->_backgroundColor;
    }

    /**
     * Set the background color (as ARGB) used for empty spaces
     * if ratio has changed between original and new image
     *
     * @param int $r  Red value (0 to 255)
     * @param int $g  Green value (0 to 255)
     * @param int $b  Blue value (0 to 255)
     * @param int $a  Alpha value (0 to 127, 127 = fully transparent)
     */
    public function setBackgroundColorRGBA($r = 0, $g = 0, $b = 0, $a = 0)
    {
        $this->_backgroundColor[0] = $r;
        $this->_backgroundColor[1] = $g;
        $this->_backgroundColor[2] = $b;
        $this->_backgroundColor[3] = $a;

        // Validate RGB color values (0 to 255 allowed)
        for ($i = 0; $i < 3; $i++) {
            if ($this->_backgroundColor[$i] < 0)
                $this->_backgroundColor[$i] = 0;
            elseif ($this->_backgroundColor[$i] > 255)
                $this->_backgroundColor[$i] = 255;
        }

        // Validate Alpha value (0 to 127 allowed)
        if ($this->_backgroundColor[3] < 0)
            $this->_backgroundColor[3] = 0;
        elseif ($this->_backgroundColor[3] > 127)
            $this->_backgroundColor[3] = 127;
    }

    /**
     * load the image from file path given in constructor
     *
     * @throws BadFunctionCallException  exit_imagetype or php_mbstring extensions not loaded
     * @throws InvalidArgumentException  not supported image type given
     */
    private function loadImage()
    {
        if (!function_exists('exif_imagetype'))
            throw new BadFunctionCallException('Function "exif_imagetype" not available! Please load extensions "php_exif" and "php_mbstring"!');

        switch (exif_imagetype($this->_imageSource)) {
            case IMAGETYPE_JPEG:
            case IMAGETYPE_JPEG2000:
                $this->_sourceImage = imagecreatefromjpeg($this->_imageSource);
                break;
            case IMAGETYPE_PNG:
                $this->_sourceImage = imagecreatefrompng($this->_imageSource);
                break;
            default:
                throw new InvalidArgumentException('Unknown Image File Format!');
        }

        // read the current image width and height
        $this->_sourceWidth = imagesx($this->_sourceImage);
        $this->_sourceHeight = imagesy($this->_sourceImage);
    }
}