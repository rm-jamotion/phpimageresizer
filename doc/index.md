---
layout: start
title: ImageResizer
---

ImageResizer
============

Inhalt:
-------

- Allgemein
{% for page in site.pages %}{% if page.categories == 'Allgemein' %}
  - [{{ page.title }}]({{ site.baseurl }}{{ page.url }})
{% endif %}{% endfor %}

- Verschiedenes
{% for page in site.pages %}{% if page.categories != 'Allgemein' %}
  - [{{ page.title }}]({{ site.baseurl }}{{ page.url }})
{% endif %}{% endfor %}

News:
-----

{% for post in posts reversed %}{% if post.url %}
  - [{{ post.title }}]({{ site.baseurl }}{{ post.url }})
{% endif %}{% endfor %}
